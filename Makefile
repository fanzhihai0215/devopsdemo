# Makefile for demo project
#
# Targets:
#   all: Builds the code
#   build: Builds the code
#   clean_binary: cleans the code
#
#

# Common
# Develop flag
#
export KEEPERURL=http://$(KEEPER_IP):$(KEEPER_PORTS)
export KEEPER_API_PREFIX=api/v1
IMAGEPREFIX=demo

# Base shell parameters
SHELL := /bin/bash
BUILDPATH=$(CURDIR)
MAKEPATH=$(BUILDPATH)/make
MAKEWORKPATH=$(MAKEPATH)
SRCPATH= src

# docker parameters
DOCKERCMD=$(shell which docker)
DOCKERBUILD=$(DOCKERCMD) build
DOCKERTAG=$(DOCKERCMD) tag
DOCKERPUSH=$(DOCKERCMD) push
DOCKERRMIMAGE=$(DOCKERCMD) rmi
DOCKERCOMPOSECMD=$(shell which docker-compose)

DOCKERCOMPOSEFILEPATH=$(MAKEWORKPATH)
DOCKERCOMPOSEFILENAME=docker-compose.yml
DOCKERCOMPOSEUIFILENAME=docker-compose.frontendbuilder.yml

# image parameters
export FRONTENDBUILDERIMAGE=frontend-builder
export TAVERNIMAGE=python:tavern2
export MAVENBUILDIMAGE=maven:3.6.1-jdk-8-slim

export FRONTENDBUILDERIMAGEURL=$(CI_REGISTRY)/$(IMAGEPREFIX)/$(FRONTENDBUILDERIMAGE)
export TAVERNIMAGEURL=$(CI_REGISTRY)/$(IMAGEPREFIX)/$(TAVERNIMAGE)
export MAVENBUILDIMAGEURL=$(CI_REGISTRY)/$(IMAGEPREFIX)/$(MAVENBUILDIMAGE)

# prepare parameters
PREPARECMD=prepare

# MAVEN parameters
MAVENCMD=mvn
MAVENBUILD=$(MAVENCMD) -Dmaven.test.skip=true clean package
MAVENIMGBASEPATH=/usr/src

# MYSQL parameters
export MYSQL_IP=db
export MYSQL_PORT=3306
export SONARQUBE_DATABASE=demo
export SONARQUBE_DATASOURCE_URL=jdbc:mysql://$(MYSQL_IP):$(MYSQL_PORT)/$(SONARQUBE_DATABASE)

#package 
GITTAG=$(shell git describe --exact-match --tags $(git log -n1 --pretty='%h') 2>/dev/null)
GITBRANCH=$(shell git branch | grep '*' | awk '{print $2}')
GITTAGVERSION=$(shell git describe --exact-match --tags $(git log -n1 --pretty='%h') 2>/dev/null || git rev-list HEAD -n 1 | cut -c 1-8 || echo UNKNOWN)
VERSIONFILE=VERSION
VERSIONTAG=$(GITTAGVERSION)

# Package lists
# TOPLEVEL_PKG := .
INT_LIST := backend
IMG_LIST := backend db frontend

# List building
COMPILEALL_LIST = $(foreach int, $(INT_LIST), $(SRCPATH)/$(int))

COMPILE_LIST = $(foreach int, $(COMPILEALL_LIST), $(int)_compile)
CLEAN_LIST = $(foreach int, $(COMPILEALL_LIST), $(int)_clean)
PKG_LIST = $(foreach int, $(IMG_LIST), $(IMAGEPREFIX)_$(int):$(VERSIONTAG))

BUILDALL_LIST = $(foreach int, $(IMG_LIST), container/$(int))
BUILD_LIST = $(foreach int, $(BUILDALL_LIST), $(int)_build)
RMIMG_LIST = $(foreach int, $(BUILDALL_LIST), $(int)_rmi)
PUSHIMG_LIST = $(foreach int, $(BUILDALL_LIST), $(int)_push)

# All are .PHONY for now because dependencyness is hard
.PHONY: deploy $(CLEAN_LIST) $(COMPILE_LIST) $(BUILD_LIST) $(BUILDALL_LIST) $(RMIMG_LIST) $(PUSHIMG_LIST)

all: compile 
compile: $(COMPILE_LIST) compile_ui
cleanbinary: $(CLEAN_LIST)

version:
	@echo $(VERSIONTAG)
	@echo $(VERSIONTAG) > $(VERSIONFILE)

compile_ui:
	$(DOCKERCOMPOSECMD) -f $(MAKEWORKPATH)/$(DOCKERCOMPOSEUIFILENAME) up
	$(DOCKERCOMPOSECMD) -f $(MAKEWORKPATH)/$(DOCKERCOMPOSEUIFILENAME) down

$(COMPILE_LIST): %_compile:
	$(DOCKERCMD) run --rm -v $(BUILDPATH)/src/$(subst src/,,$*)/settings.xml:/usr/share/maven/ref/settings.xml -v $(BUILDPATH)/src/$(subst src/,,$*):$(MAVENIMGBASEPATH)/$(subst src/,,$*) \
					-w $(MAVENIMGBASEPATH)/$(subst src/,,$*) $(MAVENBUILDIMAGEURL) $(MAVENBUILD)

$(CLEAN_LIST): %_clean:
	rm -rf $(SRCPATH)/$(subst src/,,$*)/{target,logs}

build: version $(BUILD_LIST)
push: $(PUSHIMG_LIST)
cleanimage: $(RMIMG_LIST)

$(BUILD_LIST): %_build: 
	$(DOCKERBUILD) -f $(MAKEWORKPATH)/$*/Dockerfile . -t $(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG)

$(RMIMG_LIST): %_rmi:
	$(DOCKERRMIMAGE) -f $(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG)

$(PUSHIMG_LIST): %_push:
	$(DOCKERTAG) $(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG) $(REGISTRY)/$(IMAGEPREFIX)/$(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG)
	$(DOCKERPUSH) $(REGISTRY)/$(IMAGEPREFIX)/$(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG)
	curl -X POST "http://10.224.158.10:5001/api/v1/notes/$(CI_PROJECT_PATH)?name=image-info&sha=$(CI_COMMIT_SHA)" \
	-H 'Content-Type: application/json' \
	-d '{"IMAGE_INFO": "'"$(REGISTRY)/$(IMAGEPREFIX)/$(IMAGEPREFIX)_$(subst container/,,$*):$(VERSIONTAG)"'", "TYPE": "'"$(REGISTRY_TYPE)"'"}'
prepare:
	@echo "preparing..."
	@$(MAKEPATH)/$(PREPARECMD)
	@echo "Done."

junittest_prepare:
	@echo "sonarQube-junitTest Preparing..."
	@make/scripts/junittest_prepare.sh

db:
	$(DOCKERCMD) run --rm -d -e MYSQL_ROOT_PASSWORD=$(MYSQL_PASSWORD) \
	--name db --network=test_network $(IMAGEPREFIX)_db:$(VERSIONTAG)

sonarqube:
	@make/scripts/sonarqube.sh
	curl -X POST "$(KEEPERURL)/$(KEEPER_API_PREFIX)/issues/per-sonarqube?sonarqube_token=$(SONARQUBE_TOKEN)&sonarqube_project_name=$(SONARQUBE_PROJECT_NAME)&created_in_last=20d"\

apitest_prepare:
	@echo "API Test Preparing..."
	@make/scripts/apitest_prepare.sh

apitest:
	@make/scripts/apitest.sh

apitest_writeback:
	@make/scripts/apitest_writeback.sh

start: prepare_composefile
	@echo "loading Board images..."
	$(DOCKERCOMPOSECMD) -f $(DOCKERCOMPOSEFILEPATH)/$(DOCKERCOMPOSEFILENAME) up -d
	@echo "Start complete. You can visit Board now."

down:
	@echo "stoping Board instance..."
	$(DOCKERCOMPOSECMD) -f $(DOCKERCOMPOSEFILEPATH)/$(DOCKERCOMPOSEFILENAME) down -v
	@echo "Done."

prepare_composefile:
	@cp $(MAKEWORKPATH)/docker-compose.tpl $(MAKEWORKPATH)/docker-compose.yml
	@sed -i "s/__version__/$(VERSIONTAG)/g" $(MAKEWORKPATH)/docker-compose.yml

deploy:
	@deploy/scripts/deploy.sh $(VERSIONTAG) $(IMAGEPREFIX)	$(REGISTRY)

release:
	@deploy/scripts/release.sh $(IMAGEPREFIX)  $(REGISTRY)

cleanall: cleanbinary cleanimage

clean:
	@echo "  make cleanall:         remove binaries and demo images"
	@echo "  make cleanbinary:      remove demo binaries"
	@echo "  make cleanimage:       remove demo images"

