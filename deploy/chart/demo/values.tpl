# Default values for demo.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

nameOverride: ""
fullnameOverride: ""
imagePullSecretName: "release"

frontend:
  name: frontend
  replicaCount: 1

  image:
    repository: __REPOSITORY__/demo_frontend
    tag: __version__
    pullPolicy: Always

  service:
    type: NodePort
    port: 80


  resources: {}

  nodeSelector: {}

  tolerations: []

  affinity: {}

backend:
  name: backend
  replicaCount: 1

  image:
    repository: __REPOSITORY__/demo_backend
    tag: __version__
    pullPolicy: Always

  service:
    type: ClusterIP
    port: 80


  resources: {}

  nodeSelector: {}

  tolerations: []

  affinity: {}

db:
  name: db
  replicaCount: 1
  username: __DB_USERNAME__
  password: __DB_PASSWORD__

  image:
    repository: __REPOSITORY__/demo_db
    tag: __version__
    pullPolicy: Always

  service:
    type: ClusterIP
    port: 80


  resources: {}

  nodeSelector: {}

  tolerations: []

  affinity: {}

