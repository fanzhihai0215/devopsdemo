#!/bin/bash
set -e -x

VERSIONTAG=$1
IMAGEPREFIX=$2
REGISTRY=$3

cp deploy/chart/demo/values.tpl deploy/chart/demo/values.yaml
sed -i -e "s/__version__/${VERSIONTAG}/g" -e "s|__REPOSITORY__|${REGISTRY}/${IMAGEPREFIX}|g" \
    -e "s/__DB_USERNAME__/${MYSQL_USERNAME}/g" -e "s|__DB_PASSWORD__|${MYSQL_PASSWORD}|g" deploy/chart/demo/values.yaml
helm install --name ${CI_ENVIRONMENT_SLUG}-${VERSIONTAG} deploy/chart/demo/

infos=$(helm get notes ${CI_ENVIRONMENT_SLUG}-${VERSIONTAG} |grep export)
eval $infos
echo "${CI_ENVIRONMENT_SLUG}-${VERSIONTAG} location: http://${NODE_IP}:${NODE_PORT}"

#invoke gitlab webhook
curl -sS -X POST ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/commits/${CI_COMMIT_SHA}/comments -H "PRIVATE-TOKEN: ${CI_TOKEN}" -d note="Application's URL is http://${NODE_IP}:${NODE_PORT}"
