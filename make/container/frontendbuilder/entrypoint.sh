#!/bin/bash
set -e
cp -R /frontend_src/* /frontend_resource/.
cd /frontend_resource
cat package.json

echo -e "Current mode is:${MODE}\n"
function prod(){
    echo -e "Begin executing prod"
    rm -rf /frontend_src/www
    rm -rf www/
    npm run build-prod
    cp -R www /frontend_src
    echo -e "End executing prod"
}

function test(){
    echo -e "Begin executing test"
    rm -rf converage/
    rm -rf /frontend_src/coverage
    npm run test > testresult.log
    cat testresult.log
    cp testresult.log /frontend_src
    cp -R coverage /frontend_src
    echo -e "End executing test"
}

case ${MODE} in
    prod)
        prod
        exit 0
    ;;
    test)
        test
        exit 0
    ;;
esac

