version: '2'
services:
  db:
    image: demo_db:__version__
    restart: always
    env_file:
      - ../make/config/db/env
    networks:
      - demo
  backend:
    image: demo_backend:__version__
    restart: always
    env_file:
      - ../make/config/backend/env
    networks:
      - demo
    links:
      - db
  frontend:
    image: demo_frontend:__version__
    networks:
      - demo
    restart: always
    volumes:
      - ../make/config/frontend/nginx.conf:/etc/nginx/nginx.conf:z
    ports: 
      - 10080:80
    links:
      - backend
networks:
  demo:
    external: false

