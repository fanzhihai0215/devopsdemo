#!/bin/bash

set -e -x
docker-compose -f make/docker-compose.apitest.yml up
code=$(docker inspect make_apitest_1 -f "{{.State.ExitCode}}")
#docker-compose -f make/docker-compose.apitest.yml down
exit $code
