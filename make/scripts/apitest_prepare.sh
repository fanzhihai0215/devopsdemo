#!/bin/bash

set -e -x

docker pull ${FRONTENDBUILDERIMAGEURL}
docker pull ${TAVERNIMAGEURL}
docker tag ${TAVERNIMAGEURL} python:tavern
set +x
echo "docker run --rm -v `pwd`:/data -e DATASOURCE_USERNAME=${MYSQL_USERNAME} -e DATASOURCE_PASSWORD=*** ${TAVERNIMAGEURL} /data/make/prepare"
docker run --rm -v `pwd`:/data -e DATASOURCE_USERNAME=${MYSQL_USERNAME} -e DATASOURCE_PASSWORD=${MYSQL_PASSWORD} ${TAVERNIMAGEURL} /data/make/prepare
