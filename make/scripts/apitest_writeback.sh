#!/bin/bash

set -e -x

cd test/apitest/tavern
curl -X POST \
"${KEEPERURL}/${KEEPER_API_PREFIX}/artifacts/upload?project_name=${CI_PROJECT_PATH}&job_id=${CI_JOB_ID}" \
-H "content-type: multipart/form-data" \
-F "artifact=@api_test_result.tar.gz"
curl -X POST \
"${KEEPERURL}/${KEEPER_API_PREFIX}/notes/${CI_PROJECT_PATH}?name=api-test-passed&sha=${CI_COMMIT_SHA}" \
-H 'Content-Type: application/json' \
-d '{"CI_PROJECT_PATH": "'"${CI_PROJECT_PATH}"'", "CI_JOB_ID": "'"${CI_JOB_ID}"'"}'

