#!/bin/bash

set -e -x
docker pull ${TAVERNIMAGEURL}
docker network create --driver bridge test_network
set +x
echo "docker run --rm -v `pwd`:/data -e DATASOURCE_USERNAME=${MYSQL_USERNAME} -e DATASOURCE_PASSWORD=*** ${TAVERNIMAGEURL} /data/make/prepare"
docker run --rm -v `pwd`:/data -e DATASOURCE_USERNAME=${MYSQL_USERNAME} -e DATASOURCE_PASSWORD=${MYSQL_PASSWORD} ${TAVERNIMAGEURL} /data/make/prepare

