#!/bin/bash

set -e

echo "docker run --rm -v $(pwd):${MAVENIMGBASEPATH}/mymaven -v /root/maven-repository:/root/.m2 --network=test_network -e DATASOURCE_URL=${SONARQUBE_DATASOURCE_URL} -e DATASOURCE_USERNAME=${MYSQL_USERNAME} -e DATASOURCE_PASSWORD=*** -e DATASOURCE_DRIVER=com.mysql.jdbc.Driver -w ${MAVENIMGBASEPATH}/mymaven ${MAVENBUILDIMAGEURL} mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.1.1688:sonar -f src/backend/pom.xml -s src/backend/settings.xml -Dsonar.host.url=${SONARQUBE_URL} -Dsonar.login=${SONARQUBE_TOKEN}  -Dsonar.jacoco.reportPaths=src/backend/target/coverage-reports/jacoco-unit.exec -Dsonar.projectName=${SONARQUBE_PROJECT_NAME} -Dsonar.projectKey=${SONARQUBE_PROJECT_NAME} -Dsonar.projectVersion=${CI_COMMIT_SHA} -Dsonar.gitlab.project_id=${CI_PROJECT_PATH} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME} -Dsonar.gitlab.url=${GITLAB_URL} -Dsonar.gitlab.user_token=${CI_TOKEN} -Dsonar.gitlab.comment_no_issue=true -X -Dsonar.gitlab.ci_merge_request_iid=${CI_MERGE_REQUEST_IID} -Dsonar.gitlab.merge_request_discussion=true "

docker run --rm -v $(pwd):${MAVENIMGBASEPATH}/mymaven \
-v /root/maven-repository:/root/.m2 --network=test_network \
-e DATASOURCE_URL=${SONARQUBE_DATASOURCE_URL} \
-e DATASOURCE_USERNAME=${MYSQL_USERNAME} \
-e DATASOURCE_PASSWORD=${MYSQL_PASSWORD} \
-e DATASOURCE_DRIVER=com.mysql.jdbc.Driver \
-w ${MAVENIMGBASEPATH}/mymaven ${MAVENBUILDIMAGEURL} \
mvn verify org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.1.1688:sonar -f src/backend/pom.xml -s src/backend/settings.xml \
-Dsonar.host.url=${SONARQUBE_URL} \
-Dsonar.login=${SONARQUBE_TOKEN}  \
-Dsonar.jacoco.reportPaths=src/backend/target/coverage-reports/jacoco-unit.exec \
-Dsonar.projectName=${SONARQUBE_PROJECT_NAME} \
-Dsonar.projectKey=${SONARQUBE_PROJECT_NAME} \
-Dsonar.projectVersion=${CI_COMMIT_SHA} \
-Dsonar.gitlab.project_id=${CI_PROJECT_PATH} \
-Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} \
-Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME} \
-Dsonar.gitlab.url=${GITLAB_URL} \
-Dsonar.gitlab.user_token=${CI_TOKEN} \
-Dsonar.gitlab.comment_no_issue=true -X \
-Dsonar.gitlab.ci_merge_request_iid=${CI_MERGE_REQUEST_IID} \
-Dsonar.gitlab.merge_request_discussion=true 
