package org.spring.springboot.controller;

import org.spring.springboot.domain.Message;
import org.spring.springboot.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by bysocket on 07/02/2017.
 */
@RestController
public class MessageRestController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/demo/message", method = RequestMethod.GET)
    public Message findMessage() {
        return messageService.findMessage().get(0);
    }
}
