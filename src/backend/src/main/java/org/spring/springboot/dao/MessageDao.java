package org.spring.springboot.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.spring.springboot.domain.Message;

/**
 * DAO 接口类
 *
 * Created by bysocket on 07/02/2017.
 */
public interface MessageDao {

    /**
     * 查询信息
     *
     * 
     */
	List<Message> find();
}
