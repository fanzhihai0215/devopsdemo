package org.spring.springboot.domain;

/**
 * 实体类
 *
 * Created by bysocket on 07/02/2017.
 */
public class Message {

    /**
     * 编号
     */
    private int id;

    /**
     * 信息
     */
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
