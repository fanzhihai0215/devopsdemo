package org.spring.springboot.service;

import java.util.List;

import org.spring.springboot.domain.Message;

/**
 * 逻辑接口类
 *
 * Created by bysocket on 07/02/2017.
 */
public interface MessageService {

    /**
     * 查询信息
     * 
     */
	List<Message> findMessage();
}
