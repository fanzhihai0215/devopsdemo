package org.spring.springboot.service.impl;

import java.util.List;

import org.spring.springboot.dao.MessageDao;
import org.spring.springboot.domain.Message;
import org.spring.springboot.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 逻辑实现类
 *
 * Created by bysocket on 07/02/2017.
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    public List<Message> findMessage() {
        return messageDao.find();
    }

}
