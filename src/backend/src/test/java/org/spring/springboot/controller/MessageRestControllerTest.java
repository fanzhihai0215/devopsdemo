package org.spring.springboot.controller;

import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spring.springboot.controller.MessageRestController;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageRestControllerTest {

	private String message;
	
	@Resource
	private	MessageRestController messageRestController;
	
	@Before
	public void setUp(){
		message = "hello world!";
	}
	
	@Test
	public void findMessage() {
		System.out.println(messageRestController.findMessage());
		assertEquals(message,messageRestController.findMessage().getMessage());

	}

}
