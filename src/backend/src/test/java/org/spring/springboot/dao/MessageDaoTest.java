package org.spring.springboot.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.spring.springboot.dao.MessageDao;
import org.spring.springboot.domain.Message;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageDaoTest {

	@Resource
	private MessageDao dao;
	
	@Test
	public void testfind() {
		List<Message> list = dao.find();
		assertThat(list.get(0).getMessage()).isNotEmpty();
	}
}
