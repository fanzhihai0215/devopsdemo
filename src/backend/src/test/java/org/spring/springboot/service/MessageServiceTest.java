package org.spring.springboot.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spring.springboot.dao.MessageDao;
import org.spring.springboot.domain.Message;
import org.spring.springboot.service.MessageService;
import org.spring.springboot.service.impl.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageServiceTest {

	@Resource
	private MessageService messageService;
	@Test
	public void test() {
		List<Message> messages=messageService.findMessage();
		Assert.assertEquals(1,messages.get(0).getId());
		Assert.assertEquals("hello world!",messages.get(0).getMessage());
	}

}