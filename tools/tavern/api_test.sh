#!/bin/bash


function util_done()
{
    ret=`curl $url -o /dev/null -s -w '%{http_code}'`
    while [ "$ret" != "200" ] 
    do 
    ret=`curl $url -o /dev/null -s -w '%{http_code}'`
    done
    tmp=`py.test test_api.tavern.yaml -v --html=api_test_result/report.html`
    tar zcvf api_test_result.tar.gz api_test_result

}

function init()
{
        url=$1
        sleeptime=5
        util_done $url
}
cd /usr/src/api/tavern
init $1
